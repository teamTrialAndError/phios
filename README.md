PhiOS - Phi as in Golden Ratio
======

##About##
If I'd knew, I wouldn't write this sentense.

Actually it's mainly for educational purpose. So it can be buggy, or not work at all. It probably has very limited feature set, if any.

##Build##
* Clone the repo
* Make a cdroot and bin directories
* Download SysLinux
	* Copy isolinux.bin, libcom32.c32, ldlinux.c32 into cdroot dir
	* Copy mboot.c32 into cdroot dir
	* Create syslinux.cfg file, and structure it as in example.
* Install NASM, GCC, LD, mkisofs (if on linux, or if on Windows/OS X use any other appropriate tool to create bootable ISO images)
* Use build.sh script to build the kernel (Or if on Windows translate it into batch script)
* Now that you have an ISO, you can either use VM or real hardware to run the kernel. (Please note, we don't give any warranty. It might not even work. Use it on your own risk.)

syslinux.cfg example
```
default PhiOS

label PhiOS
	say Booting PhiOS
	kernel mboot.c32
	append kernel.bin
```

##Install##
It's a liveCD atm, no install required.

##LICENSE##
Please check the LICENSE.txt

##Who?##
Who made this mess?
We did! Who are we?

* @redpandaua
* @yuraSniper