#!/bin/bash

echo Clearing previous build...
rm -rf bin
rm cdroot/kernel.bin
mkdir bin

echo Building...
nasm -g -f elf -o bin/kernel.o src/kernel.asm
gcc -m32 src/kernelCommon.c src/kernelMain.c src/kernelBasicIO.c bin/kernel.o -o bin/kernel.bin -T linker.ld -nostartfiles -nostdlib -masm=intel

#echo Linking...
#ld -m elf_i386 -T linker.ld -o bin/kernel.bin bin/kernel.o bin/kernelMain.o

echo Creating ISO image...
cp bin/kernel.bin cdroot/
mkisofs -o testos.iso -b isolinux.bin -c boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table cdroot