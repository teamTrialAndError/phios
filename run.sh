#!/bin/bash

echo Running QEMU...
qemu-system-i386 -enable-kvm -display sdl -cpu host -smp 1 -serial file:serial.log -m 32M -vga std -boot d -cdrom testos.iso