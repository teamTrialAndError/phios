bits 32

global KernelStart:function

extern main

section .text

; Multiboot Header
multibootSignature dd 464367618
multibootFlags dd 3
multibootChecksum dd -464367621
multibootGraphicsRuntime_VbeModeInfoAddr dd 2147483647
multibootGraphicsRuntime_VbeControlInfoAddr dd 2147483647
multibootGraphicsRuntime_VbeMode dd 2147483647
multibootInfo_Memory_High dd 0
multibootInfo_Memory_Low dd 0

multibootInfo_Structure dd 0

kernelStackEnd:
	times 65535 db 0
kernelStackStart:

GTD:
	db 0, 0, 0, 0, 0, 0, 0, 0			; Offset: 0  - Null selector - required 
	db 255, 255, 0, 0, 0, 0x9A, 0xCF, 0	; Offset: 8  - KM Code selector - covers the entire 4GiB address range
	db 255, 255, 0, 0, 0, 0x92, 0xCF, 0	; Offset: 16 - KM Data selector - covers the entire 4GiB address range
	db 255, 255, 0, 0, 0, 0xFA, 0xCF, 0	; Offset: 24 - UM Code selector - covers the entire 4GiB address range
	db 255, 255, 0, 0, 0, 0xF2, 0xCF, 0	; Offset: 32 - UM Data selector - covers the entire 4GiB address range

GDTPointer: db 39, 0, 0, 0, 0, 0

KernelStart:
	cli

	; Check if multiboot
	; EBX = MultiBoot Info Structure Pointer
	; EAX = 0x2BADBOO2

	mov ecx, 0x2BADB002
	cmp ecx, eax
	jne HandleNoMultiboot

	; Multiboot info
	mov dword[multibootInfo_Structure], ebx
	add ebx, 0x4
	mov eax, [ebx]
	mov dword[multibootInfo_Memory_Low], eax
	add ebx, 0x4
	mov eax, [ebx]
	mov dword[multibootInfo_Memory_High], eax

	; Switch to protected mode
	mov eax, cr0
	or eax, 1
	mov cr0, eax

	; Set stack pointer
	mov esp, kernelStackStart

	; Laod GTD
	mov dword [GDTPointer + 2], GTD
	mov dword eax, GDTPointer
	lgdt [eax]

	; Set data segments
	mov dword eax, 0x10
	mov word ds, eax
	mov word es, eax
	mov word fs, eax
	mov word gs, eax
	mov word ss, eax

	; Force reload of code segment
	jmp 8:BootFlushCsGDT

BootFlushCsGDT:
	mov eax, 0xB8000
	mov bx, 0xA000
	mov ecx, 2000
	
	push dword[multibootInfo_Structure]
	call main
	jmp Halt

HandleNoMultiboot:
	mov eax, 0xB8000
	mov bx, 0xC000
	mov ecx, 2000

	.fillScreenFailure:
		mov word[eax], bx
		add eax, 2
	loop .fillScreenFailure

Halt:
	cli
	hlt
	jmp Halt