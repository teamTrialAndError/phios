#include "kernelBasicIO.h"
#include "kernelCommon.h"

unsigned short* DisplayMemoryPtr = (unsigned short*)0xB8000;
unsigned int DisplayCursorX = 0;
unsigned int DisplayCursorY = 0;
unsigned int DisplaySize = 2000;
unsigned char DisplayBackgroudColor = 0x0;
unsigned char DisplayForegroudColor = 0xF;

unsigned short SerialPort = 0x3F8;

unsigned char ColorsToByte(unsigned char bgColor, unsigned char fgColor)
{
	return ((bgColor << 4) & 0xF0) | (fgColor & 0x0F);
}

unsigned short ColorChar(char c, unsigned char color)
{
	return (((unsigned short)color) << 8) | c;
}

unsigned int CursorXYToFlat(unsigned int x, unsigned int y)
{
	return y * 80 + x;
}

void IncCursorY()
{
	DisplayCursorY++;
	DisplayCursorX = 0;
	if(DisplayCursorY > 24)
	{
		DisplayCursorY = 24;
		ScrollScreen(1);
	}

	UpdateVGACursor();
}

void IncCursorX()
{
	DisplayCursorX++;
	if(DisplayCursorX > 79)
	{
		IncCursorY();
	}

	UpdateVGACursor();
}

void UpdateVGACursor()
{
	SetVGACursor(DisplayCursorX, DisplayCursorY);
}

void SetVGACursor(unsigned int x, unsigned int y)
{
	unsigned short position = CursorXYToFlat(x, y);

	PortOutByte(0x3D4, 0x0F);
    PortOutByte(0x3D5, (unsigned char)(position & 0xFF));
    PortOutByte(0x3D4, 0x0E);
    PortOutByte(0x3D5, (unsigned char )((position >> 8) & 0xFF));
}

void PrintChar(char c)
{
	unsigned char color = ColorsToByte(DisplayBackgroudColor, DisplayForegroudColor);

	if(c == '\t')
	{
		for(int i = 0; i < 4; i++)
		{
			DisplayMemoryPtr[CursorXYToFlat(DisplayCursorX, DisplayCursorY)] = ColorChar(' ', color);
			IncCursorX();
		}

		return;
	}

	if(c == '\n')
	{
		IncCursorY();
		return;
	}

	DisplayMemoryPtr[CursorXYToFlat(DisplayCursorX, DisplayCursorY)] = ColorChar(c, color);
	IncCursorX();
}

void Print(char* string)
{
	while(*string != '\0')
	{
		PrintChar(*string);
		string++;
	}
}

void PrintLn(char* string)
{
	Print(string);
	PrintChar('\n');
}

void PrintDec(int num)
{
	if(num == 0)
	{
		PrintChar('0');
		return;
	}

	char buffer[25];

	if(num < 0)
	{
		PrintChar('-');
		num = -num;
	}

	int i = 0;
	while (num > 0) 
	{
		int digit = num % 10;

		char digitChar = 48 + (char)digit;
		buffer[i] = digitChar;

		i++;
		num /= 10;
	}

	for(int j = i - 1; j >= 0; j--)
	{
		PrintChar(buffer[j]);
	}
}

void PrintHex(int num)
{
	if(num == 0)
	{
		Print("0x0");
		return;
	}

	char buffer[20];

	if(num < 0)
	{
		PrintChar('-');
		num = -num;
	}

	int i = 0;
	while (num > 0) 
	{
 		int digit = num % 16;
 		char digitChar = '\0';

 		if(digit < 10)
 		{
 			digitChar = 48 + (char)digit;
 			buffer[i] = digitChar;
 		}
 		else
 		{
 			digitChar = 'A' + (char)(digit - 10);
 			buffer[i] = digitChar;
 		}

 		i++;
 		num /= 16;
	}

	Print("0x");
	for(int j = i - 1; j >= 0; j--)
	{
		PrintChar(buffer[j]);
	}
}

void ClearScreen()
{
	unsigned char color = ColorsToByte(DisplayBackgroudColor, DisplayForegroudColor);

	unsigned int i = 0;
	while(i < DisplaySize)
	{
		DisplayMemoryPtr[i++] = (((unsigned short)color) << 8) | '\0'; 
	}
}

void ScrollScreen(unsigned int lines)
{
	char* buffer = (char*)DisplayMemoryPtr + 80 * 2 * lines;
	MemCopy(buffer, DisplayMemoryPtr, (DisplaySize - 80 * lines) * 2);
}

void InitSerial() 
{
	PortOutByte(SerialPort + 1, 0x00);	// Disable all interrupts
	PortOutByte(SerialPort + 3, 0x80);	// Enable DLAB (set baud rate divisor)
	PortOutByte(SerialPort + 0, 0x03);	// Set divisor to 3 (lo byte) 38400 baud
	PortOutByte(SerialPort + 1, 0x00);	//                  (hi byte)
	PortOutByte(SerialPort + 3, 0x03);	// 8 bits, no parity, one stop bit
	PortOutByte(SerialPort + 2, 0xC7);	// Enable FIFO, clear them, with 14-byte threshold
	PortOutByte(SerialPort + 4, 0x0B);	// IRQs enabled, RTS/DSR set
}

int IsSerialTransmitEmpty()
{
	return PortInByte(SerialPort + 5) & 0x20;
}

void SendSerialChar(char c)
{
	while (IsSerialTransmitEmpty() == 0);
	
	PortOutByte(SerialPort, c);
}

void SendSerialString(char* str)
{
	while(*str != '\0')
	{
		SendSerialChar(*str);
		str++;
	}
	
	SendSerialChar('\0');
}