#ifndef KERNEL_BASIC_IO_H
#define KERNEL_BASIC_IO_H

enum Colors
{
	BLACK = 0x0,
	BLUE = 0x1,
	GREEN = 0x2,
	CYAN = 0x3,
	RED = 0x4,
	MAGENTA = 0x5,
	BROWN = 0x6,
	LIGHT_GRAY = 0x7,
	DARK_GRAY = 0x8,
	LIGHT_BLUE = 0x9,
	LIGHT_GREEN = 0xA,
	LIGHT_CYAN = 0xB,
	LIGHT_RED = 0xC,
	LIGHT_MAGENTA = 0xD,
	YELLOW = 0xE,
	WHITE = 0xF,
};

extern unsigned short* DisplayMemoryPtr;
extern unsigned int DisplayCursorX;
extern unsigned int DisplayCursorY;
extern unsigned int DisplaySize;
extern unsigned char DisplayBackgroudColor;
extern unsigned char DisplayForegroudColor;

extern unsigned short SerialPort;

unsigned char ColorsToByte(unsigned char bgColor, unsigned char fgColor);
unsigned short ColorChar(char c, unsigned char color);

unsigned int CursorXYToFlat(unsigned int x, unsigned int y);
void IncCursorY();
void IncCursorX();
void UpdateVGACursor();
void SetVGACursor(unsigned int x, unsigned int y);

void PrintChar(char c);
void Print(char* string);
void PrintLn(char* string);
void PrintDec(int num);
void PrintHex(int num);

void ClearScreen();
void ScrollScreen(unsigned int lines);

void InitSerial();
int IsSerialTransmitEmpty();
void SendSerialChar(char c);
void SendSerialString(char* str);

#endif