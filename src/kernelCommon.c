#include "kernelCommon.h"

void Delay(int time)
{
	while(time > 0)
	{
		time--;
	}
}

unsigned char PortInByte ( unsigned short port ) 
{
	unsigned char result;
	asm(".intel_syntax noprefix");
	asm("in al, dx" : "=a"(port) : "d"(result));
	return result;
}

void PortOutByte ( unsigned short port, unsigned char data ) 
{
	asm(".intel_syntax noprefix");
	asm("out dx, al" : "=d"(port), "=a"(data));
}

unsigned short PortInWord ( unsigned short port ) 
{
	unsigned short result;
	asm(".intel_syntax noprefix");
	asm("in ax, dx" : "=a"(port) : "d"(result));
	return result;
}

void PortOutWord ( unsigned short port, unsigned short data ) 
{
	asm(".intel_syntax noprefix");
	asm("out dx, ax" : "=d"(port), "=a"(data));
}

void MemCopy(void* src, void* dst, unsigned int size)
{
	for (unsigned int i = 0; i < size; i++)
		((char*)dst)[i] = ((char*)src)[i];
}