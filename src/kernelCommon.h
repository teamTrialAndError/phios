#ifndef KERNEL_COMMON_H
#define KERNEL_COMMON_H

void Delay(int time);

unsigned char PortInByte ( unsigned short port );
void PortOutByte ( unsigned short port, unsigned char data );
unsigned short PortInWord ( unsigned short port );
void PortOutWord ( unsigned short port, unsigned short data );

void MemCopy(void* src, void* dst, unsigned int size);

#endif