#include "kernelCommon.h"
#include "kernelBasicIO.h"
#include "multiboot.h"

void main(multiboot_info_t* mbd) 
{
	ClearScreen();
	SetVGACursor(0, 0);

	DisplayBackgroudColor = DARK_GRAY;
	DisplayForegroudColor = LIGHT_GREEN;

	ClearScreen();

	char* welcomeMessage = "TestOS kernel loaded!";
	PrintLn(welcomeMessage);

	Print("RAM: ");
	PrintDec((int)mbd->mem_upper + (int)mbd->mem_lower);
	PrintLn(" KB");
}

void Numbers()
{
	for (unsigned int i = 0; i < 1024; i++)
	{
		PrintDec(i);
		PrintChar('\t');
		PrintHex(i);
		PrintLn("");

		Delay(0x00F0000);
	}
}